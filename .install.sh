#!/bin/bash

bundle install
./bin/rails db:create
./bin/rails db:migrate
./bin/rails db:seed