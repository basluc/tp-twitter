require 'test_helper'

class TweetTest < ActiveSupport::TestCase
  test "insertion" do
    utilisateur = utilisateurs(:basluc)
    assert_equal(utilisateur.tweets.count, 3, "Tweets != 3")
    assert(utilisateur.tweets.create({ content: "test" }))

    utilisateur = utilisateurs(:basluc)
    assert_equal(utilisateur.tweets.count, 4, "Tweets != 4")
  end
end
