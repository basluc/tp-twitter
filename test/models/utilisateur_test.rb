require 'test_helper'

class UtilisateurTest < ActiveSupport::TestCase
  test "insertion" do
    utilisateurs = Utilisateur.all
    assert_equal(utilisateurs.count, 4, "Utilisateurs != 4")
    utilisateur = Utilisateur.new({ nom: "test", prenom: "test", age: 37 })
    assert(utilisateur.valid?)
    assert(utilisateur.save, "Utilisateur test save")
    utilisateurs = Utilisateur.all
    assert_equal(utilisateurs.count, 5, "Utilisateurs après insertion != 5")
  end
end
