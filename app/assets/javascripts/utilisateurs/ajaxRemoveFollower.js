// = require jquery3
// = require popper


const AJAX_REMOVE = {
    html: "#ajaxRendering",
    event: ".remove-follower",

    init() {
        this.attachEvent();
    },

    attachEvent() {
        $(this.event).on("click", (item) => {
            console.log("test");
            
            let id = $(item.currentTarget).data("id");
            let follower = $(item.currentTarget).data("follower");

            this.ajax(id, follower);
        });

        $(this.event).hover((item) => {
            $(item.currentTarget).removeClass("fa fa-heart").addClass("far fa-heart")
        }, (item) => {
            $(item.currentTarget).removeClass("far fa-heart").addClass("fa fa-heart")
        });
    },

    ajax(id, follower) {
        $.ajax({
            type: "delete",
            url: id + "/remove/follower",
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            data: { id: id, follower: follower },
            dataType: "html"
        }).done((data) => {
            $(this.html).html(data);
            this.attachEvent();
        });
    }
}

$(document).ready(function() {
    AJAX_REMOVE.init();
});