// = require jquery3
// = require popper


const AJAX_ADD = {
    html: "#ajaxRendering",
    event: ".add-follower",

    init() {
        this.attachEvent();
    },

    attachEvent() {
        $(this.event).on("click", (item) => {
            console.log("test");
            
            let id = $(item.currentTarget).data("id");
            let follower = $(item.currentTarget).data("follower");

            this.ajax(id, follower);
        });

        $(this.event).hover((item) => {
            $(item.currentTarget).removeClass("far fa-heart").addClass("fa fa-heart")
        }, (item) => {
            $(item.currentTarget).removeClass("fa fa-heart").addClass("far fa-heart")
        });
    },

    ajax(id, follower) {
        $.ajax({
            type: "post",
            url: id + "/add/follower",
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            data: { id: id, follower: follower },
            dataType: "html"
        }).done((data) => {
            $(this.html).html(data);
            this.attachEvent();
        });
    }
}

$(document).ready(function() {
    AJAX_ADD.init();
});