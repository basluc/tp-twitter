// = require jquery3
// = require popper

AJAX_AVIS_DELETE = null;

function attachEvent() {
    $("i.user-delete").on('click', function() {
        let id = parseInt($(this).data("id"));
        let path = "/utilisateurs/" + id + "/utilisateur_delete";
        console.log(path);
        if(AJAX_AVIS_DELETE) {
            AJAX_AVIS_DELETE.abort();
        }
    
        AJAX_AVIS_DELETE = $.ajax({
            url: path,
            dataType: "html",
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            data: {id: id},
            method: "DELETE"
        }).done(function(data, textStatus, jqXHR) {
            $("#liste-users").html(data);
            attachEvent();
        }).always(function(data, textStatus, jqXHR) {
            AJAX_AVIS_DELETE = null;
        });
    });
}


$("turbolinks:load").ready(function(){
    attachEvent();
});


