class TweetsController < ApplicationController
    def index
    end
    
    def new
    end
    
    def create
        @utilisateur = Utilisateur.find(params[:utilisateur_id])
        @tweet = @utilisateur.tweets.create(tweet_params)
        redirect_to utilisateur_path(@utilisateur)
    end
    
    def show
    end
    
    def edit
    end
      
    
    def update
    end
    
    def destroy
    end
      
    private 
    def tweet_params
        params.require(:tweet).permit(:content)
        
    end
end
