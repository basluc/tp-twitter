class UtilisateursController < ApplicationController
  def index
    @utilisateurs = Utilisateur.all
  end

  def new
    @utilisateur = Utilisateur.new
  end

  def create
    # render plain: params[:utilisateur].inspect
    @utilisateur = Utilisateur.new(utilisateur_params)
    if @utilisateur.save
      redirect_to @utilisateur
    else
      render 'new'
    end
  end

  def show
    @utilisateur = Utilisateur.find(params[:id])
  end

  def liste_tweets
    @utilisateur = Utilisateur.find(params[:id])
    @tweets = @utilisateur.tweets
 
    render partial: 'liste_tweets'
  end

  def liste_add_follows
    @followers = Utilisateur.find_by_sql("
      SELECT * From utilisateurs u
      WHERE u.id not in (
            SELECT id From utilisateurs u2
            INNER JOIN follows f ON f.follower_id = u2.id
            WHERE f.followed_id = #{ params[:id] })
      AND u.id <> #{ params[:id] }
    ")
    @utilisateur = Utilisateur.find(params[:id])
     
    render partial: 'liste_follows'
  end

  def view_followers
    @followers = Utilisateur.find_by_sql("
        SELECT * From utilisateurs u
        INNER JOIN follows f ON f.follower_id = u.id
        WHERE f.followed_id = #{ params[:id] }
      ")

    @utilisateur = Utilisateur.find(params[:id])
    render partial: 'view_followers'
  end
  

  def add_follower
    @follower = Utilisateur.find(params[:follower])
    @utilisateur = Utilisateur.find(params[:id])
    @follower = Follow.new({ "follower" => @follower, "followed"  => @utilisateur })
    @follower.save
    @followers = Utilisateur.find_by_sql("
      SELECT * From utilisateurs u
      WHERE u.id not in (
            SELECT id From utilisateurs u2
            INNER JOIN follows f ON f.follower_id = u2.id
            WHERE f.followed_id = #{ params[:id] })
      AND u.id <> #{ params[:id] }
    ")

    render partial: 'ajax_follows'
  end

  def remove_follower
    # Follow.where(followed_id: params[:id], follower_id: params[:follower]).destroy
    ActiveRecord::Base.connection.execute("
        DELETE FROM follows
        WHERE followed_id = #{ params[:id] }
        AND follower_id = #{ params[:follower] }
      ")

    @followers = Utilisateur.find_by_sql("
      SELECT * From utilisateurs u
      INNER JOIN follows f ON f.follower_id = u.id
      WHERE f.followed_id = #{ params[:id] }
    ")
    
    @utilisateur = Utilisateur.find(params[:id])

    render partial: 'ajax_view_followers'
  end
  
  

  def utilisateur_delete
    @utilisateur = Utilisateur.find(params[:id])
    @utilisateur.destroy

    @utilisateurs = Utilisateur.all
    
    render partial: 'liste_utilisateurs'
  end
  

  def edit
    @utilisateur = Utilisateur.find(params[:id])
  end
  

  def update
    @utilisateur = Utilisateur.find(params[:id])
    
    if @utilisateur.update(utilisateur_params)
      redirect_to @utilisateur
    else 
      render 'edit'
    end
  end

  def destroy
    @utilisateur = Utilisateur.find(params[:id])
    @utilisateur.destroy

    redirect_to utilisateurs_path
  end
  
  private 
  def utilisateur_params
    params.require(:utilisateur).permit(:nom, :prenom, :age)
  end
  
end


