class Tweet < ApplicationRecord
  belongs_to :utilisateur
  validates :content, presence: true
end