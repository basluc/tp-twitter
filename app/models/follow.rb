class Follow < ApplicationRecord
    belongs_to :follower, class_name: 'Utilisateur', :foreign_key => "follower_id"
    belongs_to :followed, class_name: 'Utilisateur', :foreign_key => "followed_id"
end
