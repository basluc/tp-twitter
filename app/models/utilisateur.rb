class Utilisateur < ApplicationRecord
    has_many :tweets, dependent: :delete_all
    has_many :follower, foreign_key: :follower_id, class_name: "Follow"
    has_many :follower, foreign_key: :followed_id, class_name: "Follow"

    has_many :followers, :through => :relations_to, :source => :follower
    has_many :followeds, :through => :relations_from, :source => :followed
    # belongs_to :utilisateur, required: false
    validates :prenom, presence: true
    validates :age, presence: true  
end
