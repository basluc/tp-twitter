# README

Pour commencer faites un `git clone https://gitlab.com/basluc/tp-twitter.git` dans le dossier de votre projet.
Pour fonctionner le projet a besoins de nodejs via un `apt update && apt install node`

Ensuite un : `cd tp-twitter`, puis avec une console bash : `bash .install.sh`.
Cette commande installe les bundles et met en place la DB.

Vous pouvez lancer votre serveur rails puis vous rendre sur la page index via <http://localhost:3000>.

Si vous souhaitez lancer un test faites un : `bash .test.sh`. Vous pouvez ensuite remettre la base pour une utilisation normal avec : `bash .normal.sh`. 
