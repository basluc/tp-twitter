class CreateFollows < ActiveRecord::Migration[5.2]
  def change
    create_table :follows, primary_key: [:follower_id, :followed_id] do |t|
      t.column "follower_id",  :integer, :null => false 
      t.column "followed_id", :integer, :null => false 
      t.timestamps
      # CONSTRAINT follows_primary_keys PRIMARY KEY (follower_id, followed_id)
    end
    # execute "ALTER TABLE follows ADD PRIMARY KEY (follower_id, followed_id);"
  end
end
