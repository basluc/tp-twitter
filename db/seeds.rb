# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

utilisateurs = Utilisateur.create([
        { nom: 'basluc', prenom: 'basluc', age: 69, tweets: Tweet.create([{content: 'test'}, {content: 'test'}]) }, 
        { nom: 'bibi', prenom: '30', age: 30, tweets: Tweet.create([{content: 'test'}, {content: 'test'}])  }, 
        { nom: 'boss', prenom: 'poche', age: 40, tweets: Tweet.create([{content: 'test'}, {content: 'test'}])  },
        { nom: 'test', prenom: 'test', age: 19, tweets: Tweet.create([{content: 'test'}, {content: 'test'}])  },
        { nom: 'ouuu', prenom: 'test', age: 20, tweets: Tweet.create([{content: 'test'}, {content: 'test'}])  },
        { nom: 'pierre', prenom: 'jack', age: 35, tweets: Tweet.create([{content: 'test'}, {content: 'test'}])  },
    ])