Rails.application.routes.draw do
    get 'utilisateurs/index'

    resources :utilisateurs do
        resources :tweets
        member do
            get "liste_tweets"
            delete "utilisateur_delete"
            # post "add_follower"
        end

        resources :utilisateurs
        member do
            get "liste_add_follows"
            get "view_followers"
            post "/add/follower", to: 'utilisateurs#add_follower'
            delete "/remove/follower", to: 'utilisateurs#remove_follower'
        end

    end

    root to: 'utilisateurs#index'
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end